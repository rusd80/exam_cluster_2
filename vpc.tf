#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#

resource "aws_vpc" "flapp" {
  cidr_block = "172.17.0.0/16"

  tags = tomap({
    "Name"                                      = "terraform-eks-flapp-node",
    "kubernetes.io/cluster/${var.cluster-name}" = "shared",
  })
}

resource "aws_subnet" "flapp" {
  count = 2

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "172.17.${count.index}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.flapp.id

  tags = tomap({
    "Name"                                      = "terraform-eks-node-flask",
    "kubernetes.io/cluster/${var.cluster-name}" = "shared",
  })
}

resource "aws_internet_gateway" "flapp" {
  vpc_id = aws_vpc.flapp.id

  tags = {
    Name = "terraform-eks-gw"
  }
}

resource "aws_route_table" "flapp" {
  vpc_id = aws_vpc.flapp.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.flapp.id
  }
}

resource "aws_route_table_association" "flapp" {
  count = 2

  subnet_id      = aws_subnet.flapp.*.id[count.index]
  route_table_id = aws_route_table.flapp.id
}